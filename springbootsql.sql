/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50515
Source Host           : localhost:3306
Source Database       : springboot

Target Server Type    : MYSQL
Target Server Version : 50515
File Encoding         : 65001

Date: 2019-02-13 19:56:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `manage`
-- ----------------------------
DROP TABLE IF EXISTS `manage`;
CREATE TABLE `manage` (
  `id` varchar(100) NOT NULL,
  `name` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of manage
-- ----------------------------
INSERT INTO `manage` VALUES ('1001', 'lin', '123');
INSERT INTO `manage` VALUES ('1002', 'ouyang', '123');

-- ----------------------------
-- Table structure for `person`
-- ----------------------------
DROP TABLE IF EXISTS `person`;
CREATE TABLE `person` (
  `id` int(11) NOT NULL,
  `age` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of person
-- ----------------------------
INSERT INTO `person` VALUES ('1001', '25', '???');
INSERT INTO `person` VALUES ('1002', '25', '欧阳莹一');
INSERT INTO `person` VALUES ('1003', '20', 'linyongbin');
INSERT INTO `person` VALUES ('1005', '20', '叫兽');
INSERT INTO `person` VALUES ('1006', '20', '白客');

-- ----------------------------
-- Table structure for `t_money`
-- ----------------------------
DROP TABLE IF EXISTS `t_money`;
CREATE TABLE `t_money` (
  `id` int(40) NOT NULL AUTO_INCREMENT,
  `resourse` varchar(30) DEFAULT NULL,
  `time` varchar(50) DEFAULT NULL,
  `money` int(20) DEFAULT NULL,
  `remarks` varchar(70) DEFAULT NULL,
  `totalmoney` int(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10025 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_money
-- ----------------------------
INSERT INTO `t_money` VALUES ('8', null, null, null, null, '3');
INSERT INTO `t_money` VALUES ('11', null, null, '688', 'j', null);
INSERT INTO `t_money` VALUES ('1004', 'sjjj', '2019-02-12 03:21:32', '22', '22', '10355');
INSERT INTO `t_money` VALUES ('1005', '房间收入', '2019-02-13 06:12:05', '499', '豪华双人房', '12491');
INSERT INTO `t_money` VALUES ('10002', 'fafff', '2019-02-12 03:15:19', '222', '222', '10222');
INSERT INTO `t_money` VALUES ('10004', '房间收入', '2019-02-12 03:32:29', '120', '豪华双人房', '10275');
INSERT INTO `t_money` VALUES ('10005', 'sss', '2019-02-12 03:16:00', '111', '222', '10333');
INSERT INTO `t_money` VALUES ('10008', '房间收入', '2019-02-12 02:55:21', '200', '经济双人房', '10000');
INSERT INTO `t_money` VALUES ('10011', '房间收入', '2019-02-12 03:35:41', '499', '豪华双人房', '10774');
INSERT INTO `t_money` VALUES ('10018', '房间收入', '2019-02-13 04:36:45', '499', '经济单人房', '11273');
INSERT INTO `t_money` VALUES ('10019', '房间收入', '2019-02-13 04:39:04', '499', '豪华单人房', '11772');
INSERT INTO `t_money` VALUES ('10020', '房间收入', '2019-02-13 04:41:25', '220', '豪华单人房', '11992');
INSERT INTO `t_money` VALUES ('10021', 'ddd', 'dd', '33', 'dd', '33');
INSERT INTO `t_money` VALUES ('10023', '房间收入', '2019-02-13 07:20:11', '120', '豪华单人房', '153');
INSERT INTO `t_money` VALUES ('10024', '房间收入', '2019-02-13 07:27:48', '120', '豪华单人房', '153');

-- ----------------------------
-- Table structure for `t_order`
-- ----------------------------
DROP TABLE IF EXISTS `t_order`;
CREATE TABLE `t_order` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `intime` varchar(100) NOT NULL,
  `outtime` varchar(100) NOT NULL,
  `roomtype` varchar(150) NOT NULL,
  `payif` int(20) NOT NULL,
  `monney` int(50) NOT NULL,
  `roomNum` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10041 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_order
-- ----------------------------
INSERT INTO `t_order` VALUES ('10011', 'admin', '02/04/2019', '02/20/2019', '经济双人房', '1', '500', '1802');
INSERT INTO `t_order` VALUES ('10016', '测试', '02/04/2019', '02/12/2019', '豪华双人房', '1', '2000', '1802');
INSERT INTO `t_order` VALUES ('10017', '方法', '02/12/2019', '02/12/2019', '经济单人房', '1', '500', '1802');
INSERT INTO `t_order` VALUES ('10018', '成功', '02/04/2019', '02/12/2019', '豪华双人房', '1', '2000', '1802');
INSERT INTO `t_order` VALUES ('10019', '啊啊', '02/04/2019', '02/12/2019', '豪华单人房', '1', '500', '1802');
INSERT INTO `t_order` VALUES ('10020', '刘青云', '02/20/2019', '02/12/2019', '特价房', '1', '200', '1906');
INSERT INTO `t_order` VALUES ('10021', '猪佩奇', '02/19/2019', '02/20/2019', '经济双人房', '1', '500', '0');
INSERT INTO `t_order` VALUES ('10022', '林勇彬', '02/08/2019', '02/12/2019', '豪华单人房', '1', '1550', '1802');
INSERT INTO `t_order` VALUES ('10023', '猪乔治', '02/08/2019', '02/12/2019', '豪华单人房', '1', '1550', '1602');
INSERT INTO `t_order` VALUES ('10024', '猪爸爸', '02/04/2019', '02/12/2019', '经济双人房', '1', '1550', '1802');
INSERT INTO `t_order` VALUES ('10027', '林勇彬', '02/06/2019', '02/07/2019', '豪华单人房', '0', '0', '0');
INSERT INTO `t_order` VALUES ('10028', '曾悦楷', '02/07/2019', '02/08/2019', '特价房', '1', '500', '1802');
INSERT INTO `t_order` VALUES ('10029', '李佳仪', '02/04/2019', '02/20/2019', '豪华双人房', '1', '5200', '520');
INSERT INTO `t_order` VALUES ('10030', '小猪猪', '02/04/2019', '02/12/2019', '豪华单人房', '1', '2000', '1001');
INSERT INTO `t_order` VALUES ('10031', '李世明', '02/05/2019', '02/06/2019', '豪华单人房', '1', '2000', '1002');
INSERT INTO `t_order` VALUES ('10032', '是是是', '02/05/2019', '02/07/2019', '豪华双人房', '1', '120', '1003');
INSERT INTO `t_order` VALUES ('10033', '快快快', '02/06/2019', '02/05/2019', '特价房', '1', '79', '1008');
INSERT INTO `t_order` VALUES ('10034', '我', '02/05/2019', '02/12/2019', '豪华双人房', '1', '120', '1004');
INSERT INTO `t_order` VALUES ('10035', '你', '02/06/2019', '02/08/2019', '豪华双人房', '1', '499', '2001');
INSERT INTO `t_order` VALUES ('10036', '斤斤计较', '02/12/2019', '02/12/2019', '经济单人房', '1', '499', '2002');
INSERT INTO `t_order` VALUES ('10037', '嗡嗡嗡', '02/12/2019', '02/14/2019', '豪华单人房', '1', '499', '2003');
INSERT INTO `t_order` VALUES ('10038', '钢铁侠', '02/05/2019', '02/07/2019', '豪华双人房', '1', '499', '1001');
INSERT INTO `t_order` VALUES ('10039', 'mm', '02/05/2019', '02/07/2019', '豪华单人房', '1', '120', '1003');
INSERT INTO `t_order` VALUES ('10040', 'cc', '02/05/2019', '02/09/2019', '豪华单人房', '1', '120', '1004');

-- ----------------------------
-- Table structure for `t_room`
-- ----------------------------
DROP TABLE IF EXISTS `t_room`;
CREATE TABLE `t_room` (
  `id` varchar(30) NOT NULL,
  `roomtype` varchar(25) NOT NULL,
  `price` varchar(30) NOT NULL,
  `status` varchar(20) NOT NULL,
  `window` varchar(20) NOT NULL,
  `bashroom` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_room
-- ----------------------------
INSERT INTO `t_room` VALUES ('1001', '豪华双人房', '499', '1', '1', '1');
INSERT INTO `t_room` VALUES ('1002', '豪华单人房', '299', '0', '1', '1');
INSERT INTO `t_room` VALUES ('1003', '经济单人房', '120', '1', '1', '1');
INSERT INTO `t_room` VALUES ('1004', '经济单人房', '120', '1', '1', '1');
INSERT INTO `t_room` VALUES ('1005', '经济双人房', '220', '0', '1', '1');
INSERT INTO `t_room` VALUES ('1006', '特价房', '89', '0', '0', '1');
INSERT INTO `t_room` VALUES ('1007', '特价房', '99', '0', '1', '1');
INSERT INTO `t_room` VALUES ('1008', '特价房', '79', '0', '0', '0');
INSERT INTO `t_room` VALUES ('2001', '豪华双人房', '499', '0', '0', '1');
INSERT INTO `t_room` VALUES ('2002', '豪华双人房', '499', '0', '0', '1');
INSERT INTO `t_room` VALUES ('2003', '豪华双人房', '499', '0', '0', '1');

-- ----------------------------
-- Table structure for `t_student`
-- ----------------------------
DROP TABLE IF EXISTS `t_student`;
CREATE TABLE `t_student` (
  `id` varchar(100) NOT NULL DEFAULT '',
  `name` varchar(250) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `adress` varchar(250) DEFAULT NULL,
  `school` varchar(200) DEFAULT NULL,
  `eron` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_student
-- ----------------------------
INSERT INTO `t_student` VALUES ('1', '1', '1', '1', '1', '1');
INSERT INTO `t_student` VALUES ('10', '10', '1', '1', '1', '1');
INSERT INTO `t_student` VALUES ('10001', '林勇彬', '123456', '广东省揭阳市空港经济区', '计算机学院', '2016');
INSERT INTO `t_student` VALUES ('10002', '习近平', '123456', '广东省揭阳市空港经济区', '1', '1');
INSERT INTO `t_student` VALUES ('10003', '汪洋', '123456', '广东省揭阳市空港经济区', '2', '2');
INSERT INTO `t_student` VALUES ('10005', '张惠', '123456', '广东省揭阳市空港经济区', 'sss', 'ss');
INSERT INTO `t_student` VALUES ('10006', '琳琳', '123456', '广东省揭阳市空港经济区', 'sss', 'ss');
INSERT INTO `t_student` VALUES ('10007', '李佳怡', '123456', '广东省揭阳市空港经济区', 'sss', 'ss');
INSERT INTO `t_student` VALUES ('10008', '邓佳倩', '123456', '广东省揭阳市空港经济区', 'sss', 'ss');
INSERT INTO `t_student` VALUES ('10009', '王雯雯', '123456', '广东省揭阳市空港经济区', 'sss', 'ss');
INSERT INTO `t_student` VALUES ('10010', '王雯雯', '123456', '广东省揭阳市空港经济区', 'sss', 'ss');
INSERT INTO `t_student` VALUES ('10011', '鲁班七号', '123456', '王者峡谷', '王者学院', '2016');
INSERT INTO `t_student` VALUES ('10012', '韩信', '123456', '王者峡谷', '王者学院', '2016');
INSERT INTO `t_student` VALUES ('2', '2', '2', '2', '2', '2');
INSERT INTO `t_student` VALUES ('3', '3', '3', '3', '3', '3');
INSERT INTO `t_student` VALUES ('4', '4', '4', '4', '4', '4');
INSERT INTO `t_student` VALUES ('5', '5', '5', '5', '5', '5');
INSERT INTO `t_student` VALUES ('6', '6', '6', '6', '6', '6');
INSERT INTO `t_student` VALUES ('7', '7', '7', '7', '7', '7');
INSERT INTO `t_student` VALUES ('8', '8', '8', '8', '8', '8');
INSERT INTO `t_student` VALUES ('9', '9', '9', '9', '9', '9');

-- ----------------------------
-- Table structure for `t_user`
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `name` varchar(100) NOT NULL,
  `idcard` varchar(50) NOT NULL,
  PRIMARY KEY (`idcard`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of t_user
-- ----------------------------
