package com.lf.demo.service;

import com.lf.demo.mapper.RoomMapper;
import com.lf.demo.model.Room;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomService {
    @Autowired
    private RoomMapper roomMapper;

    /**
     * 查找所有的房间
     * @return
     */
    public List<Room> selectAllRoom(){
        return roomMapper.selectAll();
    }

    public int updateRoomStatus(String id,String status){
        return roomMapper.updateRoomStatus(status,id);
    }
}
