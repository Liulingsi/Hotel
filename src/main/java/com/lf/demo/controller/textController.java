package com.lf.demo.controller;

import com.lf.demo.mapper.ManageMapper;
import com.lf.demo.mapper.OrderMapper;
import com.lf.demo.mapper.StudentMMapper;
import com.lf.demo.model.Manage;
import com.lf.demo.service.UsersService;
import com.lf.demo.util.Dateutil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class textController {
    @Autowired
    private ManageMapper manageMapper;
    @Autowired
    private StudentMMapper studentMapper;
    @Autowired
    private OrderMapper orderMapper;
    @Autowired
    private UsersService usersService;

    @GetMapping(value = "/")
    public String hotel(){
        return "hotel";
    }
    @GetMapping(value = "/login")
    public String login(){
        return "welcome";
    }
    @GetMapping(value = "/manage/student")
    public String manageStudent1(){
        return "studentList";
    }
    @GetMapping(value = "/user")
    public String user(){
        return "user";
    }
    @GetMapping(value = "/dashboard")
    public String dashboard(){
        return "dashboard";
    }
    @GetMapping(value = "/maps")
    public String maps(){
        return "maps";
    }
    @GetMapping(value = "/upgrade")
    public String upgrade(){
        return "upgrade";
    }
    @GetMapping(value = "/typography")
    public String typography(){
        return "typography";
    }
    @GetMapping(value = "/template")
    public String template(){
        return "template";
    }
    @GetMapping(value = "/table")
    public String table(){
        return "table";
    }
    @GetMapping(value = "/notifications")
    public String notifications(){
        return "notifications";
    }
    @GetMapping(value = "/icons")
    public String icons(){
        return "icons";
    }
    @GetMapping(value = "/1")
    public String icons1(){
        return "thetext";
    }
    @GetMapping(value = "/login1")
    public String login1(){
        return "login1";
    }
    @GetMapping(value = "/login3")
    public String login3(){
        return "login3";
    }
    @GetMapping(value = "/reginst")
    public String reginst(){
        return "reginst";
    }
    /**
     * 管理员登录
     *
     * @param username
     * @param password
     * @return
     */
    @PostMapping(value = "/login")
    public String login1(HttpServletRequest request, @RequestParam("userName") String username, @RequestParam("password") String password){
        if(username==null||password==null||username.length()<=0||password.length()<=0){
            return "redirect:/login1";
        }
        String password1=manageMapper.selectPassword(username);
         Manage manageInfo = manageMapper.selectPerson(username);
        if(password1==null||password1.length()<=0){
            return "redirect:/login3";
        }
        if(password1.equals(password)&&(manageInfo.getType().equals("管理员")||manageInfo.getType().equals("前台")||manageInfo.getType().equals("主管"))){
            System.out.println("textcontroller用户信息:userInfo---:"+manageInfo.toString());
            request.getSession().setAttribute("manageInfo",manageInfo);
            request.getSession().setAttribute("managerole",manageInfo.getType());
            return "notifications";
        }else {
            return "redirect:/login1";
        }
    }

    @GetMapping(value = "/login2")
    public String login2(){
        return "flase";
    }
    @GetMapping(value = "/manage/All")
    public List<Manage> selectAll(){
        return manageMapper.selectAll();
    }

    /**
     * 获取当前时间
     * 已测试
     * @return
     @ResponseBody
     */
    @GetMapping(value = "/getNowDate")
    @ResponseBody
    public String getNowDate(){
        return Dateutil.getDate();
    }

    @PostMapping(value = "/reginstUser")
    public String reginstUser(@RequestParam("email") String username,@RequestParam("password") String password,@RequestParam("phone") String phone,@RequestParam("passwordagain") String passwordagain){
        System.out.println("注册textController");
        int result = usersService.reginst(username,password,phone,passwordagain);
        //0注册失败，1注册成功，2用户已存在
        if(result==0){
            return "/reginstPage/0";
        }
        if(result==1){
            return "/reginstPage/1";
        }
        return "/reginstPage/2";
    }

}
