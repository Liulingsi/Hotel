package com.lf.demo.mapper;

import com.lf.demo.model.Order;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface OrderMapper {
    int insert(Order record);

    int insertSelective(Order record);

    List<Order> selectAll();

    /**
     * 更新订单付款状态
     * @param id
     * @return
     */
    int updatePay(String id);

    //String findMaxId();

    int deleteOrderById(String id);

    /**
     * 更新订单已付款金额
     * @param id
     * @param monney
     * @return
     */
    int updateOrderMonney(Integer id, Integer monney);
}