package com.lf.demo.mapper;

import com.lf.demo.model.Money;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;
@Mapper
@Component
public interface MoneyMapper {
    int deleteByPrimaryKey(String id);

    int insert(Money record);

    int insertSelective(Money record);

    Money selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Money record);

    int updateByPrimaryKey(Money record);

    int selectTotalMoney();

    List<Money> selectAll();
}