package com.lf.demo.mapper;

import com.lf.demo.model.Manage;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;
@Mapper
@Component
public interface ManageMapper {
    int insert(Manage record);

    int insertSelective(Manage record);
    String selectPassword(String username);
    List<Manage> selectAll();
    Manage selectPerson(String username);
}