package com.lf.demo.mapper;

import com.lf.demo.model.Room;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface RoomMapper {
    int insert(Room record);

    int insertSelective(Room record);

    List<Room> selectAll();

    int updateRoomStatus(String status, String id);

    int selectRoomPrice(String id);
}