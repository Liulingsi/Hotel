package com.lf.demo.mapper;

import com.lf.demo.model.StudentM;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 学生
 */
@Mapper
@Component
public interface StudentMMapper {
    int insert(StudentM record);

    int insertSelective(StudentM record);

    List<StudentM> selectAllM();

    StudentM selectByid(String id);

    String findMaxId();

    Boolean deleteStudById(String id);
}